package org.assia.fr;

import junit.framework.TestCase;

public class fractiontest extends TestCase {
	
	public boolean consultationNumTest(int num) 
	{
		int numTheo = num; 
		int numObtenu = fraction.setNu(num);
		
		if(numObtenu == numTheo) return true;
		else return false;
	}
	
	public boolean consultationDenoTest(int deno) 
	{
		int denoTheo = deno;
		int denoObtenu = fraction.setDe(deno);
		
		if(denoObtenu == denoTheo) return true;
		else return false;
	}
	
	public boolean consulTest()
	{
		double consulTheo = 2.5;
		double consulObtenu = fraction.consul(5,2);
		
		if(consulTheo == consulObtenu) return true;
		else return false;
	}
	
}
